# LENDI

   I. This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

   II. The codebase is checked and linted by standardjs: the JavaScript Standard Style (https://standardjs.com)

## LENDI SPECIFICATIONS:

    I. Create an application that retrieves data from an API and display that information as a list using both React and Redux.
    II. Write accompanying tests using Jest
    III. API endpoint: https://jsonplaceholder.typicode.com/photos
    IV. Useful links:
       a. https://facebook.github.io/react/
       b. https://github.com/facebookincubator/create-react-app
       c. https://github.com/reactjs/redux
       d. https://facebook.github.io/jest/


### MAIN REQUIREMENTS

a. Node 6.12.0 or above

b. NPM

c. The internet

### HOW TO RUN THE UNIT TESTS

  a. npm run test

### HOW TO SET UP AND RUN LENDI VIA THE COMMAND LINE (LINUX/MAC)

  a. Git clone this repository into your local machine git clone git@bitbucket.org:raymondhub/lendi.git

  b. Get into the cloned folder: cd lendi

  c. Run npm install

  d. Run the app: npm run start, the app runs on http://localhost:3000


### TECH STACK

a. React.js via create-react-app (https://github.com/facebook/create-react-app)

b. Redux.js (https://redux.js.org) and Redux-saga.js (https://redux-saga.js.org) for React state management and Redux middleware

c. JavaScript Testing Suite JEST (https://facebook.github.io/jest) for unit testing