import { call, put, takeLatest } from 'redux-saga/effects'
import { fetchPhotos } from '../apis/fetchPhotos'
import { REQUEST_API_DATA, receiveApiData } from '../actions'

function * getPhotos (action) {
  try {
    // API call
    const data = yield call(fetchPhotos)
    yield put(receiveApiData(data))
  } catch (e) {
    console.log(e)
  }
}

export function * getPhotosSaga () {
  yield takeLatest(REQUEST_API_DATA, getPhotos)
}
