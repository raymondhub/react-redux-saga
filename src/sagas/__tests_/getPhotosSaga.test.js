import { takeLatest } from 'redux-saga/effects'
import { getPhotosSaga } from '../getPhotosSaga'
import { fetchPhotos } from '../../apis/fetchPhotos'
import { REQUEST_API_DATA, receiveApiData } from '../../actions'

const gps = getPhotosSaga()

describe(`The get photos saga`, () => {
  it('it has to be a function', () => {
    expect(typeof getPhotosSaga).toBe('function')
  })
  test(`It fetches and puts CORRECT data into the get photos saga`, () => {
      function * getPhotos (action) {
        try {
          const data = yield call(fetchPhotos)
          yield put(receiveApiData(data))
        } catch (e) {
          console.log(e)
        }
      }
    expect(gps.next().value.toString()).toEqual(takeLatest(REQUEST_API_DATA, getPhotos).toString())
  })
  test(`It fetches but puts INCORRECT data into the get photos saga`, () => {
    const fetchPosts = async () => {
      try {
        const photosApi = `https://jsonplaceholder.typicode.com/posts`
        const response = await fetch(photosApi)
        const data = await response.json()
        return data
      } catch (e) {
        console.log(e)
      }
    }
    function * getPosts (action) {
      try {
        const data = yield call(fetchPosts)
        yield put(receiveApiData(data))
      } catch (e) {
        console.log(e)
      }
    }
  expect(gps.next()).not.toEqual(takeLatest(REQUEST_API_DATA, getPosts))
 })
})
