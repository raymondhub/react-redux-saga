import * as photosSaga from './getPhotosSaga'
import { all, fork } from 'redux-saga/effects'

export default function * initSagas() {
  yield all([...Object.values(photosSaga)].map(fork))
}
