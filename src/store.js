import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducers'
import initSagas from './sagas'

const sagaMiddleware = createSagaMiddleware()
// mount it on the Store
export default createStore(reducer, compose(applyMiddleware(sagaMiddleware),
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))
sagaMiddleware.run(initSagas)
