export const fetchPhotos = async () => {
  try {
    const photosApi = `https://jsonplaceholder.typicode.com/photos`
    const response = await fetch(photosApi)
    const data = await response.json()
    return data
  } catch (e) {
    console.log(e)
  }
}

