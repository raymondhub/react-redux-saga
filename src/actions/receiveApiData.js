export const RECEIVE_API_DATA = `RECEIVE_API_DATA`
export const receiveApiData = data => ({ type: RECEIVE_API_DATA, data })
