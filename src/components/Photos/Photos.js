import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Item from 'semantic-ui-react/dist/commonjs/views/Item/Item'
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon/Icon'
import { requestApiData } from '../../actions'
import './Photos.css'

class Photos extends Component {
  // constructor(props) {
  //   super(props)
  //   this.handleClick = this.handleClick.bind(this)
  // }
  
  handleClick = (data, e) => {
    e.preventDefault()
    window.location.href = data
  }
  componentDidMount() {
    this.props.requestApiData()
  }

  photos = (photo, i) =>
    <Item key={i}>
      <Item.Image className='Photo-image' 
                  size='tiny'
                  src={photo.thumbnailUrl} onClick={(e) => this.handleClick(photo.url, e)} />
      <Item.Content header={photo.title} meta={`item id: ${photo.id}`} />
    </Item>
  
  render() {
    return this.props.data.length
      ? <Item.Group>
          {this.props.data.map(this.photos)}
        </Item.Group>
      : <Icon loading name='asterisk' size='massive' />
  }
}

const mapStateToProps = (state) => ({ data: state.data })
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ requestApiData }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Photos)