import React from 'react'
import { Provider } from 'react-redux'
import store from '../store'
import Photos from './Photos/Photos'

export default () =>
  <Provider store={store}>
    <Photos />
  </Provider>